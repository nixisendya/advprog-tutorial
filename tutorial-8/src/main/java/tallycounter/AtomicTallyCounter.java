package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {
    private AtomicInteger count = new AtomicInteger();

    public void increment() {
        count.getAndIncrement();
    }

    public void decrement() {
        count.getAndDecrement();
    }

    public int value() {
        return count.intValue();
    }
}
