import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                + String.valueOf(each.calculate()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(calculate()) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints())
            + " frequent renter points";
        return result;
    }

    private double calculate() {
        double result = 0;
        Iterator<Rental> iterator = rentals.iterator();

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += each.calculate();
        }
        return result;
    }

    private int frequentRenterPoints() {
        int result = 0;
        Iterator<Rental> iterator = rentals.iterator();

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += each.frequentRenterPoints();
        }
        return result;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();

        String result = "<h1>Rental Record for <em>" + getName() + "</em></h1>\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += each.getMovie().getTitle() +":" + String.valueOf(each.calculate()) + "<br>\n";
        }

        // Add footer lines
        result += "<p>Amount owed is <em>" + String.valueOf(calculate()) + "</em></p>\n";
        result += "<p>You earned <em>" + String.valueOf(frequentRenterPoints())
            + " frequent renter points</em></p>";
        return result;
    }
}