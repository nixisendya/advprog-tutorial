class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    // Moved from Customer.java
    public double calculate() {
        double res = 0;
        switch (getMovie().getPriceCode()) {
            case Movie.REGULAR:
                res += 2;
                if (getDaysRented() > 2) {
                    res += (getDaysRented() - 2) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                res += getDaysRented() * 3;
                break;
            case Movie.CHILDREN:
                res += 1.5;
                if (getDaysRented() > 3) {
                    res += (getDaysRented() - 3) * 1.5;
                }
                break;
            default:
                break;
        }
        return res;
    }

    public int frequentRenterPoints() {
        if ((getMovie().getPriceCode() == Movie.NEW_RELEASE) && getDaysRented() > 1) {
            return 2;
        } else {
            return 1;
        }
    }
}