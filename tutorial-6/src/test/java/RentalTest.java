import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie;
    private Movie movie2;
    private Movie movie3;
    private Rental rent;
    private Rental rent2;
    private Rental rent3;

    @Before
    public void setUp() throws Exception {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);

        movie2 = new Movie("Who Killed Captain Nixi", Movie.CHILDREN);
        rent2 = new Rental(movie2, 4);

        movie3 = new Movie("Who Killed Captain Hambalaba", Movie.NEW_RELEASE);
        rent3 = new Rental(movie3, 2);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void calculate() {
        assertEquals("" + rent.calculate(), "3.5");
        assertEquals("3.0", "" + rent2.calculate());
        assertEquals("6.0", "" + rent3.calculate());
    }

    @Test
    public void frequentRenterPoints() {
        assertTrue(rent.frequentRenterPoints() == 1);

        movie.setPriceCode(Movie.NEW_RELEASE);
        assertTrue(rent.frequentRenterPoints() == 2);
    }
}