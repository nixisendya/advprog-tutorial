import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Customer customer;
    private Movie movie;
    private Movie movie2;
    private Rental rent;
    private Rental rent2;

    @Before
    public void setUp() throws Exception {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
        movie2 = new Movie("Who Killed Caption Nixi?", Movie.REGULAR);
        rent2 = new Rental(movie2, 3);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        customer.addRental(rent);
        customer.addRental(rent2);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("Amount owed is 7.0"));
        assertTrue(result.contains("2 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
    }
}