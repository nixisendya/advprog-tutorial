package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        if (salary < 200000.00) {
            throw new IllegalArgumentException();
        }
        super.name = name;
        super.salary = salary;
        super.role = "CEO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
