package tutorial.javari;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void javariAnimalNotExist() throws Exception {
        this.mockMvc.perform(get("/javari"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("error")));
    }

    @Test
    @DirtiesContext
    public void javariAnimalExist() throws Exception {
        this.mockMvc.perform(post("/javari"));
        this.mockMvc.perform(get("/javari"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].type", is("none")))
                .andExpect(jsonPath("$[0].name", is("none")))
                .andExpect(jsonPath("$[0].condition", is("HEALTHY")))
                .andExpect(jsonPath("$[0].weight", is(0.0)))
                .andExpect(jsonPath("$[0].showable", is(true)))
                .andExpect(jsonPath("$[0].gender", is("MALE")))
                .andExpect(jsonPath("$[0].length", is(0.0)));
    }

    @Test
    public void javariAnimalWithIdNotExist() throws Exception {
        this.mockMvc.perform(get("/javari/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("error")));
    }

    @Test
    @DirtiesContext
    public void javariAnimalWithIdExist() throws Exception {
        this.mockMvc.perform(post("/javari"));
        this.mockMvc.perform(get("/javari/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.type", is("none")))
                .andExpect(jsonPath("$.name", is("none")))
                .andExpect(jsonPath("$.condition", is("HEALTHY")))
                .andExpect(jsonPath("$.weight", is(0.0)))
                .andExpect(jsonPath("$.showable", is(true)))
                .andExpect(jsonPath("$.gender", is("MALE")))
                .andExpect(jsonPath("$.length", is(0.0)));
    }

    @Test
    @DirtiesContext
    public void javariDeleteAnimalWithIdExist() throws Exception {
        this.mockMvc.perform(post("/javari?name=a&type=b&condition=not healthy&weight=0&length=0&gender=female"));
        this.mockMvc.perform(delete("/javari/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.type", is("b")))
                .andExpect(jsonPath("$.name", is("a")))
                .andExpect(jsonPath("$.condition", is("SICK")))
                .andExpect(jsonPath("$.weight", is(0.0)))
                .andExpect(jsonPath("$.showable", is(false)))
                .andExpect(jsonPath("$.gender", is("FEMALE")))
                .andExpect(jsonPath("$.length", is(0.0)));
    }

    @Test
    public void javariDeleteAnimalWithIdNotExist() throws Exception {
        this.mockMvc.perform(delete("/javari/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("error")));
    }
}
