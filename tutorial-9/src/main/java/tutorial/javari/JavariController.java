package tutorial.javari;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tutorial.hello.Greeting;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@RestController
public class JavariController {

    private AtomicInteger counter = new AtomicInteger();
    private final Greeting error = new Greeting(404, "Error! Animal not found!");
    private ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
    private Animal animal;
    private ArrayList<Animal> listAnimals = new ArrayList<>();

    @RequestMapping(method = POST, value = "/javari")
    public Animal addAnimal(
            @RequestParam(value = "name", defaultValue = "none") String name,
            @RequestParam(value = "type", defaultValue = "none") String type,
            @RequestParam(value = "gender", defaultValue = "male") String gender,
            @RequestParam(value = "length", defaultValue = "0") String length,
            @RequestParam(value = "weight", defaultValue = "0") String weight,
            @RequestParam(value = "condition", defaultValue = "healthy") String condition
    ) {
        animal = new Animal(
                counter.incrementAndGet(),
                type,
                name,
                Gender.parseGender(gender),
                Double.parseDouble(length),
                Double.parseDouble(weight),
                Condition.parseCondition(condition));
        listAnimals.add(animal);
        return animal;
    }

    @RequestMapping(method = GET, value = "/javari")
    public String showAnimals() throws JsonProcessingException {
        if (listAnimals.size() <= 0) {
            return writer.writeValueAsString("error");
        }
        return writer.writeValueAsString(listAnimals);
    }

    @RequestMapping(method = GET, value = "/javari/{id}")
    public String showAnimals(@PathVariable String id) throws JsonProcessingException {
        animal = findAnimal(Integer.parseInt(id));
        if (animal == null) {
            return writer.writeValueAsString("error");
        }
        return writer.writeValueAsString(animal);
    }

    @RequestMapping(method = DELETE, value = "/javari/{id}")
    public String deleteAnimal(@PathVariable String id) throws JsonProcessingException {
        animal = deleteAnimalbyId(Integer.parseInt(id));
        if (animal == null) {
            return writer.writeValueAsString("error");
        }
        return writer.writeValueAsString(animal);
    }

    private Animal findAnimal(int id) {
        Animal ret = null;
        for (Animal animal : listAnimals) {
            if (animal.getId() == id) {
                ret = animal;
                break;
            }
        }
        return ret;
    }

    private Animal deleteAnimalbyId(int id) {
        for (int i = 0; i < listAnimals.size(); i++) {
            if (listAnimals.get(i).getId() == id) {
                return listAnimals.remove(i);
            }
        }
        return null;
    }
}
