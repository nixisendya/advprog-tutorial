package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VeggiesFunctionalityTest {

    private Veggies blackOlives;
    private Veggies eggplant;
    private Veggies garlic;
    private Veggies mushroom;
    private Veggies onion;
    private Veggies pineapple;
    private Veggies redPepper;
    private Veggies spinach;

    @Before
    public void setUp() throws Exception {
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        pineapple = new Pineapple();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void testVeggiesOutput() {
        assertEquals("Black Olives", blackOlives.toString());
        assertEquals("Eggplant", eggplant.toString());
        assertEquals("Garlic", garlic.toString());
        assertEquals("Mushrooms", mushroom.toString());
        assertEquals("Onion", onion.toString());
        assertEquals("Pineapple", pineapple.toString());
        assertEquals("Red Pepper", redPepper.toString());
        assertEquals("Spinach", spinach.toString());
    }

}
