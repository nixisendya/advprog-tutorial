package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore nyPizzaStore;

    @BeforeEach
    public void setUp() {
        nyPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void createCheesePizzaTest()
    {
        Pizza pizza = nyPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), "New York Style Cheese Pizza");
    }

    @Test
    public void createClamPizzaTest()
    {
        Pizza pizza = nyPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "New York Style Clam Pizza");
    }

    @Test
    public void createVeggiePizzaTest()
    {
        Pizza pizza = nyPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), "New York Style Veggie Pizza");
    }
}
