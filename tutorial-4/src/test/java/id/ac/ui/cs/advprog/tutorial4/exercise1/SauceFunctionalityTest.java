package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PestoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SauceFunctionalityTest {
    private Sauce marinaraSauce;
    private Sauce pestoSauce;
    private Sauce plumTomatoSauce;

    @Before
    public void setUp() throws Exception {
        marinaraSauce = new MarinaraSauce();
        pestoSauce = new PestoSauce();
        plumTomatoSauce = new PlumTomatoSauce();

    }

    @Test
    public void testSauceOutput(){
        assertEquals("Marinara Sauce",marinaraSauce.toString());
        assertEquals("Pesto Sauce",pestoSauce.toString());
        assertEquals("Tomato sauce with plum tomatoes",plumTomatoSauce.toString());
    }

}
