package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.RomanoCheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheeseFunctionalityTest {

    private Cheese cheddarCheese;
    private Cheese mozzarellaCheese;
    private Cheese parmesanCheese;
    private Cheese reggianoCheese;
    private Cheese romanoCheese;

    @Before
    public void setUp() throws Exception {
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        reggianoCheese = new ReggianoCheese();
        romanoCheese = new RomanoCheese();
    }

    @Test
    public void testCheeseOutput(){
        assertEquals("Shredded Mozzarella",mozzarellaCheese.toString());
        assertEquals("Shredded Parmesan",parmesanCheese.toString());
        assertEquals("Reggiano Cheese",reggianoCheese.toString());
        assertEquals("Romano Cheese",romanoCheese.toString());
    }

}
