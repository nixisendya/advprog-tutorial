package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class RomanoCheese implements Cheese {

    public String toString() {
        return "Romano Cheese";
    }
}
