package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        // TODO Complete me!
        // Create a new Pizza Store franchise at Depok

        PizzaStore dpkStore = new DepokPizzaStore();

        Pizza pizza2 = dpkStore.orderPizza("cheese");
        System.out.println("Arthur ordered a " + pizza + "\n");

        pizza2 = dpkStore.orderPizza("clam");
        System.out.println("Arthur ordered a " + pizza + "\n");

        pizza2 = dpkStore.orderPizza("veggie");
        System.out.println("Arthur ordered a " + pizza + "\n");

    }
}
