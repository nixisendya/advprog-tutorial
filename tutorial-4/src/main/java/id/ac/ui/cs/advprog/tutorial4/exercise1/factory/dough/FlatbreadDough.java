package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FlatbreadDough implements Dough {
    public String toString() {
        return "Flatbread Dough";
    }
}
