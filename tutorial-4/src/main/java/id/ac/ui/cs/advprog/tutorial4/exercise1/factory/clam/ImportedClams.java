package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class ImportedClams implements Clams {

    public String toString() {
        return "Imported Clams from Japan";
    }
}
