package matrix;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class MatrixMultiplicationTest {
    //TODO Implement, apply your test cases here
    private static String pathFile = "plainTextDirectory/input/matrixProblem";
    private static String problem1 = pathFile + "A/matrixProblemSet1.txt";
    private static String problem2 = pathFile + "A/matrixProblemSet2.txt";

    private double[][] matrixNotSquare;
    private double[][] matrix1;
    private double[][] matrix2;

    @Before
    public void setUp() throws IOException {
        matrixNotSquare = new double[][]{{1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}};
        matrix1 = Main.convertInputFileToMatrix(problem1, 50, 50);
        matrix2 = Main.convertInputFileToMatrix(problem2, 50, 50);
    }

    @Test
    public void testMainClassWorks() {
        assertTrue(mainClassChecker());
    }

    private static boolean mainClassChecker() {
        boolean okay = true;
        try {
            Main.main(null);
        } catch (Exception e) {
            okay = false;
        } finally {
            return okay;
        }
    }

    @Test
    public void illegalMultiplyTester() {
        boolean isIllegal = false;
        try {
            MatrixOperation.basicMultiplicationAlgorithm(matrixNotSquare, matrixNotSquare);
        } catch (InvalidMatrixSizeForMultiplicationException e) {
            isIllegal = true;
        } finally {
            assertTrue(isIllegal);
        }
    }
}
