package sorting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here

    private int[] arr1;
    private int[] arr2;
    private int searchInt;
    private int randomInt;

    @Before
    public void setUp() throws IOException {
        arr1 = Main.convertInputFileToArray();
        arr2 = arr1.clone();
        searchInt = 8975;
        randomInt = -10;
    }

    @Test
    public void testMainMethodWorks() {
        assertTrue(mainChecker());
    }

    private boolean mainChecker() {

        boolean success = true;
        try {
            Main.main(null);
        } catch (Exception e) {
            success = false;
        } finally {
            return success;
        }
    }

    @Test
    public void testAllSorter() {
        assertTrue(isSort(Sorter.slowSort(arr1)));
        assertTrue(isSort(Sorter.quickSort(arr2)));
    }

    private boolean isSort(int[] arrayInt) {
        if (arrayInt.length <= 1) {
            return true;
        }
        for (int i = 1; i < arrayInt.length; i++) {
            if (arrayInt[i - 1] > arrayInt[i]) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testAllFinder() {
        assertEquals(searchInt, Finder.slowSearch(arr1, searchInt));
        int[] arrSorted = Sorter.quickSort(arr1);
        //assertEquals(searchInt, Finder.binarySearch(arrSorted, searchInt));
    }

    @Test
    public void testIntNotFound() {
        assertEquals(-1, Finder.slowSearch(arr1, randomInt));
        int[] arrSorted = Sorter.quickSort(arr1);
        assertEquals(-1, Finder.binarySearch(arrSorted, randomInt));
    }
}
