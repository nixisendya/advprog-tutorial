package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    // More optimal sorting algorithm, using quickSort.

    public static int[] quickSort(int[] arrayInt) {
        quickSort(arrayInt, 0, arrayInt.length - 1);
        return arrayInt;
    }

    private static void quickSort(int[] arrayInt, int start, int end) {
        int i = start;
        int j = end;
        int pivot = arrayInt[start + (end - start) / 2];

        while (i <= j) {
            while (i < end && arrayInt[i] < pivot) {
                i++;
            }
            while (j > start && arrayInt[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(arrayInt, i, j);
                i++;
                j--;
            }
        }
        swap(arrayInt, i, end);
        if (start < j) {
            quickSort(arrayInt, start, j);
        }
        if (i < end) {
            quickSort(arrayInt, i, end);
        }
    }

    private static void swap(int[] arrayInt, int param1, int param2) {
        int temp = arrayInt[param1];
        arrayInt[param1] = arrayInt[param2];
        arrayInt[param2] = temp;
    }






}
