package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    // Quicker algorithm of searching using Binary Search

    public static int binarySearch(int[] arrayInt, int searchInt) {
        return binarySearch2(arrayInt, 0, arrayInt.length - 1, searchInt);
    }

    private static int binarySearch2(int[] arr, int l, int r, int x) {
        if (r >= l) {
            int mid = l + (r - l) / 2;

            // If the element is present at the middle itself
            if (arr[mid] == x) {
                return mid;
            }

            // If element is smaller than mid, then it can only be present in left subarray
            if (arr[mid] > x) {
                return binarySearch2(arr, l, mid - 1, x);
            }

            // Else the element can only be present in right subarray
            return binarySearch2(arr, mid + 1, r, x);
        }

        // We reach here when element is not present in array
        return -1;
    }
}
